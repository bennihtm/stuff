# saved
## [Bild 1 (ohne Hintergrund)](saved.png)
## [Bild 2 (mit Hintergrund)](saved_w_bg.png)

### Somethin' i dunno.
> Denn durch die Gnade seid ihr gerettet worden aufgrund des Glaubens. Dazu habt ihr selbst nichts getan, es ist Gottes Geschenk
~ [Epheser 2,8](https://www.bibleserver.com/LUT.MENG.NeÜ/Epheser2,8)

Gerettet.
Ich finde diesen Vers so schön, weil er die Core-Fundamentals des Evangeliums auf dem Punkt bringt.

1. Du bist gerettet. - Jesus ist für dich gestorben, dir *wird* nichts mehr passieren.

2. Du kannst nichts dafür (tun). - Gott allein hat dich mit seinem eizigen Sohn erkauft.
Du hast ihm nicht mal geholfen, einen guten Deal daraus zu machen ;) - trotzdem hat er dich erkauft.

3. Er hat dich nicht erkauft, und dann (wie es mir häufiger passiert 😳) nach 2 oder 3 Tagen auf den Dackboden verfrachtet.
Es ist ein Geschenk an dich, etwas was du wollen solltest.

Etwas, wofür du kämpfen solltest - aber du **kannst es dir nicht selbst erarbeiten** - das wäre Werksgerechtigkeit und wir wissen ja alle, so etwas sollte man lieber nicht anfangen.