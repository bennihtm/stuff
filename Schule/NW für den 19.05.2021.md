# Naturwissenschaften für den 19.05.2021
## Seite 36 Nummer 1
Heute kann man sich zu jeder Jahreszeit Orangen oder Erdbeeren kaufen.
Früher gab es in Laden nur Dinge die regional hergestellt wurden, wenn also keine Erdbeerzeit war, konnte man auch keine Erdbeeren kaufen.

Heute werden die Produkte die nicht regional angebaut werden können, einfach importiert.
So haben wir alle Lebensmittel, auch wenn sie bei uns gar nicht wachen könnten.

## Seite 36 Nummer 2
Es gibt viele Leute die sich im Winter Erdbeeren kaufen, bei uns sind die Felder leer - würden die Erdbeeren nicht importiert werden, gäbe es sie nicht in den Regalen.
Unsere Essegewohnheiten haben sich dahin verändert, dass man alles immer haben kann. 
Wenn es nicht regional wächst, dann wird es halt importiert.

## Seite 36 Nummer 3
Das importieren kostet Geld, aber das ist nicht der größte Preis den wir bezahlen.
Dadurch dass wir Erdbeeren im Winter haben wollen, müssen sie aus Spanien, den Niederlanden oder Italien importiert werden.
Da der Importprozess über Schiffe oder LKWs durchgeführt wird, werden Abgase ausgestoßen, die die Klimaerwärmung beschleunigen.

## Seite 37 Kasten A Nr. 1 a)-c) 
Für 25 Gramm Minisalami muss man 100 kg Weizen verfüttern.

Für 1kg Rindfleisch braucht man 9kg Getreide.

Für 25 Gramm Minisalamis braucht man 225 Gramm Rindfleisch.

## Seite 37 Kasten A Nr.2 a)-c) 
Von dem für 1kg Rindfleisch verfütterten Getreide könnten sich 8 Menschen am Tag ernähren.

Ich glaube das Veredelung Verschwendung ist, denn man könnte mit 8kg Weizen viel mehr Essen produzieren als wenn man es an ein Rind verfüttert.

## MHD
### Wofür steht die Abkürzung MHD?​
Die Abkürzung MHD steht für **M**indest**h**altbarkeits**d**atum.

### Wann dürfen Lebensmittel noch nach Ablauf des MHD verzehrt werden?​
Lebensmittel mit abgelaufenem Mindesthaltbarkeitsdatum dürfen dann noch verzehrt werden, wenn der Geruch, die Konsistenz und das Aussehen noch nicht merklich geändert haben. (Zum Beispiel Schimmelbildung auf der Oberfläche oder sauer riechende Milch)

### Wann dürfen Lebensmittel nicht mehr verzehrt werden?​
Wenn sich auf der Oberfläche merklich Schimmel gebildet hat, sich der Geruch oder die Konsistenz erheblich verändert hat, sollte man das Lebensmittel nicht mehr verzehren.