# Politik für den 20.05.2021
## Seite 22 Nummer 1: Welche Gründe nennen Nina und Louis Phillipp für ihr politisches Engagement?
1. Man kann bei Sachen mitmachen, die einen direkt betreffen.
2. Man bekommt ein viel besseres Politikverständnis.
3. Politikleistung in der Schule wird besser
4. Man kann etwas für andere tun.
5. Mit Leuten im Team zusammenarbeiten

## Seite 22 Nummer 2: Welchen der gesammelten Gründe hältst du für den überzeugensten? Stelle deine Wahl und deine Begründung dazu vor.

Ich finde, dass man bei Sachen mitarbeiten kann die einen direkt betreffen am überzeugensten.
Es gibt einige Bereiche wo ich das Gefühl habe, dass einiges falsch läuft, aber die Aufmerksamkeit eben auf etwas anderem liegt.

Deswegen finde ich das Argument sehr überzeugend, sich selbt einbringen zu können.