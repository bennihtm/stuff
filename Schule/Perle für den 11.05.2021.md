# Was sagt eigentlich die Bibel zu dem Thema Verliebtheit, Freundschaft und Ehe?


> Keiner darf sich in dieser Sache Übergriffe erlauben und seinen Bruder betrügen. Denn solche Vergehen wird der Herr selbst rächen. All das haben wir euch auch schon früher mit aller Deutlichkeit gesagt. - [1.Thessalonicher 4,6](https://www.bibleserver.com/HFA.Ne%C3%9C.MENG/1.Thessalonicher4,6)

Fremdgehen ist Sünde: Wenn man in sich in einer Beziehung / Ehe befindet darf man nicht seinen Partner betrügen.


> Gott hat uns nicht dazu berufen, ein unmoralisches, sondern ein geheiligtes Leben zu führen. - [1.Thessalonicher 4,6](https://www.bibleserver.com/HFA.Ne%C3%9C.MENG/1.Thessalonicher4,7)

Sex (umoralisches Leben) vor der Ehe ist Sünde.

> "Habt ihr nie gelesen", erwiderte Jesus, "dass Gott die Menschen von Anfang an als Mann und Frau geschaffen hat? - [Matthäus 19,4-6](https://www.bibleserver.com/HFA.Ne%C3%9C.MENG/Matth%C3%A4us19%2C4-6)

Wir wurden für die Beziehung mit dem anderen Geschlecht geschaffen.
Gleichgeschlechtliche Ehe / Homosexualität ist Sünde.

> Gott, der HERR, sagte: »Es ist nicht gut, dass der Mensch allein ist. Ich will ihm jemanden zur Seite stellen, der zu ihm passt!« - [1. Mose 2,18](https://www.bibleserver.com/HFA.Ne%C3%9C.MENG/1.Mose2%2C18)

Gott hat uns für die Beziehung geschaffen.
Wir sollen beziehungen eingehen.